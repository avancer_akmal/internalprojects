##The xlwt module cannot create .xlsx files. It is a writer for .xls files.
##C:\Users\cheri\Documents\DailyTasks   Working Dir
import os
import datetime
import xlrd
from xlutils.copy import copy as xl_copy
from xlwt import Workbook

home = os.path.expanduser('~'"\Documents\DailyTasks")
file = home+"\DailyTasks.xls"
print("your working directory should be %s" %home)
#Functions

def wrt_sheet(file,ind,msg):
    rb = xlrd.open_workbook(file, formatting_info=True)
    wb = xl_copy(rb)
    sheet = wb.get_sheet(ind-1)
    sheet.write(1,0,datetime.date.today())
    sheet.write(1,1,msg)
    wb.save(file)

def Exel(file ):
    Sheet = (input("Name the sheet to add:"))
    wb = Workbook()
    wb.add_sheet(Sheet)
    wb.save(file)

def add_sheet(file):
    # open existing workbook
    rb = xlrd.open_workbook(file, formatting_info=True)
    # make a copy of it
    wb = xl_copy(rb)
    # add sheet to workbook with existing sheets
    new_sheet = (input("Name the sheet to add (Enter 'exit' to exit):"))
    if new_sheet.casefold() in map(str.casefold,Get_sheets(file)): ## Function o/p is a list [Projects, Learnings]
        print("Given sheet already exists")
        add_sheet(file)
    elif new_sheet == "exit" or new_sheet == "Exit":
        options()
    else:
        Sheet1 = wb.add_sheet(new_sheet)
        wb.save(file)
        options()
def Get_sheets(file):
    xls = xlrd.open_workbook(file)
    Sheets= xls.sheet_names()
    return Sheets
def edit_sheet(name):
    print("Edit sheet")

def options():

    if not os.path.exists(file):
        Exel(file)
    print("1.Edit\n2.Add Sheet\n3.Exit")
    num = int(input("Select the above options..?"))
    i = 0
    if num == 1:
        print("What item you need to edit?:")
        Sheets= Get_sheets(file)
        for i in Sheets:
            print("{}:{}" .format(Sheets.index(i) +1 , i))
        num = int(input("Select the above options..?"))
        print("Updating {}".format(Sheets[num - 1]))
        msg =input("Give the data to update:")
        wrt_sheet(file, num, msg)

    elif num == 2:
        add_sheet(file)
    elif num == 3:
        print("Exiting the program.")
        exit(0)

def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
        else:
            print("Directory %s already exists" %directory )
    except OSError:
        print('Error: Creating directory. ' + directory)

#Step1:  Check the folder exist if not create one
createFolder(home)
options()
#wrt_sheet(file)

